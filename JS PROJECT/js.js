let dino = document.getElementById("dino");
let cactus = document.getElementById("cactus");
let scoreElement = document.getElementById("score");
let score = 0;
let gamePaused = false;
let isJumping = false;
function jump() {
isJumping = true; 
if (!gamePaused) {
dino.classList.add("jump");
setTimeout(function () {
dino.classList.remove("jump");
isJumping = false; 
}, 300);
}
if (score >= 200) {
    alert("Поздравляем! Вы закончили игру.");
  }
}
function gameOver() {  
dino.style.animationPlayState = "paused";
cactus.style.animationPlayState = "paused";
alert("Whoops! Game Over :( Your score: " + score);
score = 0;
scoreElement.textContent = "Score: " + score;
dino.style.animationPlayState = "running";
cactus.style.animationPlayState = "running";
dino.style.top = "143px"; 
cactus.style.left = "600px"; 
gamePaused = false; 
}

let checkAlive = setInterval(function () {
    let dinoTop = parseInt(window.getComputedStyle(dino).getPropertyValue("top"));
    let cactusx = parseInt(window.getComputedStyle(cactus).getPropertyValue("left"));

    if (cactusx > 0 && cactusx < 70 && dinoTop >= 143) {
        gameOver();
    } 
    if (cactusx > 0 && cactusx < 8 && isJumping) {
        score++;
        scoreElement.textContent = "Счёт: " + score;
        }
    }
, 50);
document.addEventListener("keydown", function (event) {
if (event.key === " " && !isJumping && !gamePaused) {
jump();
}
});

document.addEventListener("keydown", function (event) {
if (event.key === "з" || event.key === "p") {
gamePaused = !gamePaused;
if (gamePaused) {
dino.style.animationPlayState = "paused";
cactus.style.animationPlayState = "paused";
alert('Game Paused');
} else {
dino.style.animationPlayState = "running";
cactus.style.animationPlayState = "running";
}
}
});
