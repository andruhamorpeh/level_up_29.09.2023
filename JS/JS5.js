const OPTION_VALUE = {
    all: 'all',
    completed: 'completed',
    notCompleted: 'notCompleted',
}

const OPTIONS = [
    { value: OPTION_VALUE.all, text:"Всё" },
    { value: OPTION_VALUE.completed, text:"Выполненые" },
    { value: OPTION_VALUE.notCompleted, text:"Не выполненые" }
]

class Model {
    constructor(){
        this.todos = [];
        this.optionValue = OPTION_VALUE.all;
        this.completedTodo = 0;
    }

    bindTodoListChange(callback){
        this.onTodoListChanged = callback;
    }

    addTodo(todoText){
        const todo = {
            id: Date.now(),
            text: todoText,
            complete: false,
        };
        this.todos.push(todo)
        this.updateStore(this.todos)
    }

    deleteTodos(){
        this.todos = [];
        localStorage.clear("todos");
        this.updateStore(this.todos);
    }
    removeTodo(todoId){
        this.todos = this.todos.filter((todo) => todo.id !== todoId);
        this.updateStore(this.todos)
    }

    editComplete = (todoId) => {
        this.todos = this.todos.map((item) =>
        item.id === todoId ? {...item, complete: !item.complete} :item
        );

        this.updateStore(this.todos);
    }

    filterTodo(optValue) {
        const filterTodo = this.todos.filter((item) => {
            if(optValue === OPTION_VALUE.all) return true;
            if(optValue === OPTION_VALUE.completed) {
                return item.complete;
            }
            if(optionValue === OPTION_VALUE.notCompleted) {
                return !item.complete;
            }
        })

        this.updateStore(filterTodo);
    }

    searchTodo (inputSearchValue){
        const SearchTodo = this.todos.filter((item) => item.text.toLocaleLowerCase().includes(inputSearchValue.toLocaleLowerCase()));
        this.updateStore(SearchTodo);
    } 

    saveLocalStorageTodos(){
        const  jsonStringifyTodos = JSON.stringify(this.todos);
        localStorage.setItem('todos', jsonStringifyTodos);
    }

    percentCompleted(){
        const filterCompleteTodo = this.todos.filter((item) => item.complete);
        const completeResult = 100 * filterCompleteTodo.length / this.todos.length;
        this.completedTodo = completeResult;
    }

    getLocalStorageTodos() {
        const getTodos = localStorage.getItem('todos');
        const parseTodos = JSON.parse(getTodos) || [];
        this.todos = parseTodos;
        this.percentCompleted();
        this.updateStore();
    }

    updateStore(todos = this.todos){
        this.percentCompleted();
        this.onTodoListChanged(todos, this.completedTodo);
    }
}

class View {
    constructor() {
        this.app = this.getElement("#root");

        this.form = this.createElement("form");
        this.header = this.createElement("header", "header");
        this.title = this.createElement("p", "title");
        this.progressBar = this.createElement('div', 'progressbar');
        this.progressBarResult = this.createElement('div', 'progressbar_result')

        this.input = this.createElement("input", "form__input");
        this.container = this.createElement("div", "container");
        this.todoList = this.createElement("ul", "lists");
        this.inputSearch = this.createElement('input', 'input__search');
        this.inputSearchWrapper = this.createElement('div', 'input__search-wrapper');
        this.buttonAdd = this.createElement("button", "form__button");
        this.select = this.createElement("select", "select");
        this.buttonSaveLocalStorage =  this.createElement('button', "button__save-all");
        this.buttonDeleteAll = this.createElement('button', 'button__delete-all')
        this.progressBarResultPercent = this.createElement('p', 'progressbar__percent');

        this.header.append(this.title)
        this.container.append(this.progressBar,this.inputSearchWrapper,this.form, this.todoList, this.buttonSaveLocalStorage, this.buttonDeleteAll)
        this.progressBar.append(this.progressBarResult)
        this.inputSearchWrapper.append(this.select, this.inputSearch) 
        this.form.append(this.input, this.buttonAdd)
        this.app.append(this.header, this.container)

        this.title.textContent = "Todo List";
        this.input.placeholder = "Добавить задачу";
        this.input.type = "text";   
        this.inputSearch.placeholder = "Поиск";
        this.inputSearch.type = "text";
        this.buttonSaveLocalStorage.textContent = "Сохранить всё";
        this.buttonDeleteAll.textContent = "Удалить всё";

        this.buttonAdd.textContent = "Добавить";

        this.spanText = "";
        this.handleListenerSpanTag();

        for (let i=0; i<OPTIONS.length; i++){
            const opt = this.createElement("option")
            opt.value = OPTIONS[i].value
            opt.innerHTML = OPTIONS[i].text;
            this.select.appendChild(opt);
        }
    }

    getElement(selector) {
        const element = document.querySelector(selector);
        return element;
    }

    createElement(tag, className){
        const element = document.createElement(tag);
        if(className) element.classList.add(className);
        return element;
    }

    renderTodos(todos, completedTodo){
        while(this.todoList.firstChild){
            this.todoList.removeChild(this.todoList.firstChild);
        }

        if(!todos.length){
            const p = this.createElement("p", "title__hint");
            p.textContent = "Нет информации";
            this.todoList.append(p);
        } else {
            console.log(todos);
            todos.map((item) => {
                const li = this.createElement("li", "list")
                const span = this.createElement("span")
                const checkbox = this.createElement("input")
                const buttonDelete = this.createElement("button", "list__button")
                this.progressBarResult.style.width = `${completedTodo}%`

                checkbox.type = "checkbox"
                checkbox.checked = item.complete   
                buttonDelete.textContent = "Удалить"
                
                span.contentEditable = true;

                li.id = item.id;

                li.append(checkbox, span, buttonDelete);

                if(item.complete){
                    const strike = this.createElement("s");
                    strike.textContent = item.text
                    span.append(strike)
                } else {
                    span.textContent = item.text;
                }

                this.todoList.append(li);
            });
        }
    }

    get _todoText() {
        return this.input.value
    }
    resetInput = () => {
        this.input.value = ""
    };

    handleListenerSpanTag() {
        this.todoList.addEventListener("input", (event) => {
            if (event.target.className === "editable"){
                this.spanText = event.target.innerText;
            }
        })
    }
    bindAddTodo(handler){
        this.form.addEventListener("submit", (event) => {
            event.preventDefault();
            if (this._todoText){
                handler(this._todoText);
                this.resetInput();
            }
        });
    }

    bindRemoveTodo(handler) {
        this.todoList.addEventListener("click", (event) => {
            if (event.target.className === "list__button") {
                const id = parseInt(event.target.parentElement.id);
                handler(id);
            }
        });
    }

    bindToggleCheckbox(callback){
        this.todoList.addEventListener("change", (event) =>{
            if (event.target.type === "checkbox"){
                const id = parseInt(event.target.parentElement.id);
                callback(id);
            }
        });
    }

    bindEditTextInput(callback){
        this.todoList.addEventListener("focusout", (event) => {
            if (this.spantext) {
                const id = parseInt(event.target.parentElement.id)
                callback(id, this.spantext)
                this.spanText = "";
            }
        })
    }

    bindFilterTodo(callback){
        this.select.addEventListener("click", () => {
            const optionIndex = this.select.selectedIndex;
            const selectedOption = this.select.options;
            console.log(optionIndex)
        })
    }

    bindSaveLocalStorage(callback){
        this.buttonSaveLocalStorage.addEventListener("click", () =>{
            callback();
        })
    }
    bindSearchTextTodo (callback) {
        this.inputSearch.addEventListener('focusout', (e) => {
            callback(e.target.value);
        });
    }

    bindDeleteAllTodos(callback){
        this.buttonDeleteAll.addEventListener('click', () =>{
            callback();
        })
    }
}

class Controller {
    constructor(model, view){
        this.model = model
        this.view = view
        this.model.bindTodoListChange(this.onTodoListChanged)

        this.view.bindAddTodo(this.handleAddTodo);
        this.view.bindRemoveTodo(this.handleRemoveTodo);
        this.view.bindToggleCheckbox(this.handleEditToggle);
        this.view.bindEditTextInput(this.handleEditText);
        this.view.bindFilterTodo(this.handleFilterTodo);
        this.view.bindSearchTextTodo(this.handleSearchTodoText);
        this.view.bindSaveLocalStorage(this.handleSaveLocalStorage);
        this.view.bindDeleteAllTodos(this.handleDeleteAllTodos);
        this.model.getLocalStorageTodos();
        this.onTodoListChanged(this.model.todos, this.model.completedTodo);
    }

    onTodoListChanged = (todos, completedTodo) => {
        this.view.renderTodos(todos, completedTodo);
    }

    handleAddTodo = (inputText) => {
        this.model.addTodo(inputText);
    }

    handleRemoveTodo = (id) => {
        this.model.removeTodo(id);
    }

    handleEditToggle = (todoId) => {
        this.model.editComplete(todoId);
    }

    handleEditText = (todoId,text) => {
        this.model.editText(todoId,text)
    }

    handleFilterTodo = (optValue) => {
        this.model.filterTodo(optValue)
    }

    handleSearchTodoText = (value) => {
        this.model.searchTodo(value)
    }

    handleSaveLocalStorage = () => {
        this.model.saveLocalStorageTodos();
    }

    handleDeleteAllTodos = () => {
        this.model.deleteTodos();
    }
}

const app = new Controller(new Model(), new View());



