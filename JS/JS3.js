let arr1 = [1, 2, "a", "b"], arr2 = [1, "a", "b", 0, 15], arr3 = [1, 2, "aasf", "1", "123", 123]
let newArray = (arg) => arg.filter((a) => typeof a === 'number' )
console.log(newArray(arr1))
console.log(newArray(arr2))
console.log(newArray(arr3))

let phone1=[1, 2, 3, 4, 5, 6, 7, 8, 9, 0], phone2= [5, 1, 9, 5, 5, 5, 4, 4, 6, 8],
phone3= [3, 4, 5, 5, 0, 1, 2, 5, 2, 7]
function phoneNumber(number){
  let a = number.slice(0,3).join('')
  let b = number.slice(3,6).join('')
  let c = number.slice(6).join('')
  return (`${a} ${b}-${c}`)
}
console.log(phoneNumber(phone1))
console.log(phoneNumber(phone2))
console.log(phoneNumber(phone3))


let nums1=[0], nums2=[1], nums3=[], nums4=[0,1,5]
let evenOrOdd = (args) => {
    const sum = args.reduce((acc,num) => acc+num,0)
    if (sum%2===0){
        return 'even'
    } else {
        return 'odd'
    }
}
console.log(evenOrOdd(nums1))
console.log(evenOrOdd(nums2))
console.log(evenOrOdd(nums3))
console.log(evenOrOdd(nums4))


let sizes1={width: 2, length: 5, height: 1}, sizes2={width: 4, length: 2, height: 2 },
sizes3={width: 2, length: 3, height: 5 }
function volumeOfBox(arg){
    return Object.values(arg).reduce((acc,num) => acc*num,1)
}
console.log(volumeOfBox(sizes1))
console.log(volumeOfBox(sizes2))
console.log(volumeOfBox(sizes3))


let a={Emma: 71, Jack: 45, Amy: 15, Ben: 29}, b={Max: 9, Josh: 13, Sam: 48, Anne: 33}
function mostOld (arg){
    const entries = Object.entries(arg)
    const max=Math.max(...Object.values(arg))
    for (let i=0;i<entries.length;i++){
    if (entries[i].includes(max)){
        return entries[i][0]
    }
    }
}
console.log(mostOld(a))
console.log(mostOld(b))

let dna1= 'ATTAGCGCGATATACGCGTAC', dna2='CGATATA', dna3='GTCATACGACGTA'
function dnaToRna (dna){
    let Rna = ''
    const obj={A:'U',T:'A',G:'C',C:'G'}
    for (let i = 0; i <dna.length; i++) {
        Rna += obj[dna[i]];
      }
    return Rna
}
console.log(dnaToRna(dna1))
console.log(dnaToRna(dna2))
console.log(dnaToRna(dna3))


function perimeter(input) {
  const [shape, value] = input
  const num = Number(value)
  const isSquare = shape === 's'
  const isCircle = shape === 'c'
  const squarePerimeter = 4 * num
  const circlePerimeter = 6.28 * num
  return  isSquare ? squarePerimeter : isCircle ? circlePerimeter : '0'   
}
console.log(perimeter(["s", 7]))
console.log(perimeter(["c", 4]))
console.log(perimeter(["c", 9]))


let speak1= 'javascript is cool', speak2='programming is fun', speak3='become a coder'
function hackerSpeak (speak){
    let hs = ''
    let letters={a:'4',e:'3',i:'1',o:'0',s:'5'}
    for (let i=0;i<speak.length;i++){
        if(Object.keys(letters).includes(speak[i])){
            hs+=letters[speak[i]]
        } else {
            hs+=speak[i]
        }
    }
    return hs
}
console.log(hackerSpeak(speak1))
console.log(hackerSpeak(speak2))
console.log(hackerSpeak(speak3))


let nan1=[1, 2, NaN], nan2=[NaN, 1, 2, 3, 4], nan3=[0, 1, 2, 3, 4]
function findNaN (arr){
    return arr.findIndex(a => isNaN(a))
}
console.log(findNaN(nan1))
console.log(findNaN(nan2))
console.log(findNaN(nan3))


