Array.prototype.customMap = function(a){
    const newArray =[]
    for(let i=0;i<this.length;i++){
        const result=a(this[i],i,this)
        newArray.push(result)
    }
    return newArray
}
const nums=[1,2,3,4,5]
const squaredNumbers=nums.customMap(function(number){return number*number})
console.log(squaredNumbers)



Array.prototype.customFilter = function(a){
    const newArray =[]
    for(let i=0;i<this.length;i++){
        if (a(this[i],i,this)){
            newArray.push(this[i])
        }
    }
    return newArray
}
const numbers=[1,2,3,4,5]
const filteredNumbers=numbers.customFilter(function(number){return number>3})
console.log(filteredNumbers)