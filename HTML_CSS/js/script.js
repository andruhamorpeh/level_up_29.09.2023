const reviewSwiper = document.querySelector(".reviews__swiper");

if (reviewSwiper){
    new Swiper(".reviews__swiper",{
        autoHeight: true,
        loop: true,
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },
    });
}